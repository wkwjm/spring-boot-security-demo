package com.sans;

import com.sans.core.entity.SysUserEntity;
import com.sans.core.entity.SysUserRoleEntity;
import com.sans.core.service.SysUserRoleService;
import com.sans.core.service.SysUserService;
import com.sans.mq.Producer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles({"test"})
public class SpringBootSecurityApplicationTest {


    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private SysUserRoleService sysUserRoleService;
    @Autowired
    private Producer producer;

    /**
     * 注册用户
     */
    @Test
    public void contextLoads() {
        // 注册用户
        SysUserEntity sysUserEntity = new SysUserEntity();
        sysUserEntity.setUsername("sans");
        sysUserEntity.setPassword(bCryptPasswordEncoder.encode("123456"));
        // 设置用户状态
        sysUserEntity.setStatus("NORMAL");
        sysUserService.save(sysUserEntity);
        // 分配角色 1:ADMIN 2:USER
        SysUserRoleEntity sysUserRoleEntity = new SysUserRoleEntity();
        sysUserRoleEntity.setRoleId(2L);
        sysUserRoleEntity.setUserId(sysUserEntity.getUserId());
        sysUserRoleService.save(sysUserRoleEntity);
    }



    @Test
    public void amqpTest() throws InterruptedException {
        // 生产者发送消息
        producer.produce();
        // 让子弹飞一会儿
        Thread.sleep(1000);
    }


    @Test
    public void workConsumer() throws InterruptedException {
        // 生产者发送消息
        producer.work_produce();
        // 让子弹飞一会儿
        Thread.sleep(1000);
    }

    @Test
    public void publishSubcribe() throws InterruptedException {
        // 生产者发送消息
        producer.publishSubcribe();
        // 让子弹飞一会儿
        Thread.sleep(1000);
    }

    @Test
    public void routConsumer() throws InterruptedException {
        // 生产者发送消息
        producer.routConfig();
        // 让子弹飞一会儿
        Thread.sleep(1000);
    }
}