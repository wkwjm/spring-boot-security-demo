package com.sans.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @title: PS_Consumer
 * @Author 王康
 * @Date: 2022/6/30 19:15
 * @Version 1.0
 */
@Component
public class PS_Consumer {
    private final static Logger logger = LoggerFactory.getLogger(PS_Consumer.class);

    @RabbitListener(queues = "ps.queue.one")
    public void receive1(String message){
        logger.info("消费者1收到消息： " + message);
    }

    @RabbitListener(queues = "ps.queue.two")
    public void receive2(String message){
        logger.info("消费者2收到消息： " + message);
    }
}
