package com.sans.mq;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Optional;

/**
 * @title: Producer
 * @Author 王康
 * @Date: 2022/6/30 16:05
 * @Version 1.0
 */
@Component
public class Producer implements Serializable {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    private static final String EXCHANGE_NAME = "rout.exchange";
    private static final String QUEUE_NAME_ONE = "rout.queue.one";
    private static final String QUEUE_NAME_TWO = "rout.queue.two";
    /**
     * 简单模式
     */
    public void produce() {
        for (int i = 0; i < 100; i++) {
            String message = "疫情期间注意防护";
            System.out.println("乡长"+i+"说：" + message);
            rabbitTemplate.convertAndSend("notice_queue", message);
        }
    }


    /**
     * 工作队列模式 无非就是两个消费者都接收同一个队列，只需要修改一下消费者端即可：
     * 可以发现两个消费者是通过轮询来分配消息，而且每个消息只能消费一次
     */
    public void work_produce() {
        for (int i = 0; i < 100; i++) {
            String message = "疫情期间注意防护";
            System.out.println("乡长"+i+"说：" + message);
            rabbitTemplate.convertAndSend("work.queue", message);
        }
    }


    /**
     * 发布订阅模式 这个模式可以解决工作模式中消息只能消费一次的问题
     */
    public void publishSubcribe() {
        String message = "疫情期间注意防护";
        System.out.println("乡长"+"说：" + message);
        rabbitTemplate.convertAndSend("ps.exchange","", message);

        StringBuffer  SB = new StringBuffer();
        StringBuilder bb = new StringBuilder();
    }

    /**
     * 发布订阅模式 这个模式可以解决工作模式中消息只能消费一次的问题
     */
    public void routConfig() {
        String type = "0";
        if ("0".equals(type)) {
            String message = "疫情期间注意防护";
            System.out.println("乡长"+"说：" + message);
            rabbitTemplate.convertAndSend(EXCHANGE_NAME,QUEUE_NAME_ONE, message);
        }else{
            String message = "好好学习";
            System.out.println("乡长"+"说：" + message);
            rabbitTemplate.convertAndSend(EXCHANGE_NAME,QUEUE_NAME_TWO, message);
        }
    }
}
