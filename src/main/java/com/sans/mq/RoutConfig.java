package com.sans.mq;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @title: RoutConfig 路由Routing模式和发布确认模式的区别就是使用了 Routing Keys ，而前面的发布确认的Routingkeys都设置成了 “”
 * 配置类，注意交换机要使用Direct类型的交换机
 * @Author 王康
 * @Date: 2022/6/30 19:28
 * @Version 1.0
 */
@Configuration
public class RoutConfig {
    private static final String EXCHANGE_NAME = "rout.exchange";
    private static final String QUEUE_NAME_ONE = "rout.queue.one";
    private static final String QUEUE_NAME_TWO = "rout.queue.two";

    @Bean("rq1")
    public Queue queue1(){
        return  QueueBuilder.durable(QUEUE_NAME_ONE).build();
    }
    @Bean("rq2")
    public Queue queue2(){
        return  QueueBuilder.durable(QUEUE_NAME_TWO).build();
    }
    @Bean("rexchange")
    public Exchange RExchange(){
        return new DirectExchange(EXCHANGE_NAME);
    }
    @Bean
    public Binding getbind1(@Qualifier("rq1")Queue queue, @Qualifier("rexchange")Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(QUEUE_NAME_ONE).noargs();
    }
    @Bean
    public Binding getbind2(@Qualifier("rq2")Queue queue,@Qualifier("rexchange")Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(QUEUE_NAME_TWO).noargs();
    }
}
