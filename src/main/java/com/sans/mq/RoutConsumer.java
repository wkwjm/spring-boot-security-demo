package com.sans.mq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @title: RoutConsumer
 * @Author 王康
 * @Date: 2022/6/30 19:41
 * @Version 1.0
 */
@Component
@Slf4j
public class RoutConsumer {
    private static final String QUEUE_NAME_ONE = "rout.queue.one";
    private static final String QUEUE_NAME_TWO = "rout.queue.two";


    @RabbitListener(queues = QUEUE_NAME_ONE)
    public void listen1(String message){
        log.info("消费者1收到消息 ： "+message);
    }

    @RabbitListener(queues = QUEUE_NAME_TWO)
    public void listen2(String message){
        log.info("消费者2收到消息： "+message);
    }
}