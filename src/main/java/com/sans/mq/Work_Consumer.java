package com.sans.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @title: Work_Consumer
 * @Author 王康
 * @Date: 2022/6/30 17:12
 * @Version 1.0
 */
@Component
public class Work_Consumer {
    private final static Logger logger = LoggerFactory.getLogger(Work_Consumer.class);

    @RabbitListener(queues = "work.queue")
    public void receive1(String message){
        logger.info("消费者1收到的消息是"+message);
    }

    @RabbitListener(queues = "work.queue")
    public void receive2(String message){
        logger.info("消费者2收到的消息是"+message);
    }

}
