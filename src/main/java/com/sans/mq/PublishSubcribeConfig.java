package com.sans.mq;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @title: PublishSubcribeConfig 发布订阅模式 这个模式可以解决工作模式中消息只能消费一次的问题
 * @Author 王康
 * @Date: 2022/6/30 19:08
 * @Version 1.0
 */
@Configuration
public class PublishSubcribeConfig {

    private static final String EXCHANGE_NAME = "ps.exchange";
    private static final String QUEUE_NAME_ONE = "ps.queue.one";
    private static final String QUEUE_NAME_TWO = "ps.queue.two";

    @Bean("psExchange")
    public Exchange PSExchange(){
        return new FanoutExchange(EXCHANGE_NAME);
    }
    @Bean("queueOne")
    public Queue queueOne(){
        return QueueBuilder.durable(QUEUE_NAME_ONE).build();
    }
    @Bean("queueTwo")
    public Queue queueTwo(){
        return QueueBuilder.durable(QUEUE_NAME_TWO).build();
    }
    @Bean
    public Binding bindOne(@Qualifier("psExchange") Exchange exchange, @Qualifier("queueOne")Queue queue){
        return BindingBuilder.bind(queue).to(exchange).with("").noargs();

    }
    @Bean
    public Binding bindTwo(@Qualifier("psExchange") Exchange exchange, @Qualifier("queueTwo")Queue queue){
        return BindingBuilder.bind(queue).to(exchange).with("").noargs();
    }
}
