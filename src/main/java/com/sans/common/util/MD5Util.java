package com.sans.common.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * @title: MD5Util
 * @Author 王康
 * @Date: 2023/3/28 11:50
 * @Version 1.0  MD5加密工具
 */
public class MD5Util {

    public static String stringToMD5(String plainText) {
        byte[] mdBytes = null;
        try {
            mdBytes = MessageDigest.getInstance("MD5").digest(
                    plainText.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5算法不存在！");
        }
        String mdCode = new BigInteger(1, mdBytes).toString(16);

        if(mdCode.length() < 32) {
            int a = 32 - mdCode.length();
            for (int i = 0; i < a; i++) {
                mdCode = "0"+mdCode;
            }
        }
        return mdCode;            // 默认返回32位小写
    }

    public static void main(String[] args) throws Exception {
        String content = "hello,您好";
        String key = MD5Util.stringToMD5("18756968631");
        System.out.println("原文=" + content);
        String s1 = AESUtil.encrypt(content, key);
        System.out.println("加密结果=" + s1);
        System.out.println("解密结果="+AESUtil.decrypt(s1, key));


        List<String> list = new ArrayList<>(303);

        for (int i = 1; i < 100+1; i++) {
            if( i%3 == 0){
                list.add(i*3,i+"");
            }else{

            }
        }

        //System.out.println(JSON);


    }


}
