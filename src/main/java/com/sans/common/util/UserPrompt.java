package com.sans.common.util;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.IsoFields;
import java.util.HashMap;
import java.util.Map;
/**
 * @title: UserPrompt
 * @Author 王康
 * @Date: 2023/9/5 14:27
 * @Version 1.0
 */
public class UserPrompt {
    private Map<String, String> lastPromptTimeMap;

    public UserPrompt() {
        lastPromptTimeMap = new HashMap<>();
        lastPromptTimeMap.put("123456","20220508235959");
    }

    public boolean shouldPromptEveryTime(String userId) {
        return true; // 每次必弹
    }

    public boolean shouldPromptDaily(String userId) {
        String lastPromptTime = lastPromptTimeMap.getOrDefault(userId, "");
        String currentTime = getCurrentTime();
        return !currentTime.substring(0, 8).equals(lastPromptTime.substring(0, 8));
    }

    public boolean shouldPromptWeekly(String userId) {
        String lastPromptTime = lastPromptTimeMap.getOrDefault(userId, "");
        String currentTime = getCurrentTime();
        return getWeekOfYear(currentTime) != getWeekOfYear(lastPromptTime);
    }

    public boolean shouldPromptMonthly(String userId) {
        String lastPromptTime = lastPromptTimeMap.getOrDefault(userId, "");
        String currentTime = getCurrentTime();
        return !currentTime.substring(0, 6).equals(lastPromptTime.substring(0, 6));
    }

    public boolean shouldPromptYearly(String userId) {
        String lastPromptTime = lastPromptTimeMap.getOrDefault(userId, "");
        String currentTime = getCurrentTime();
        return !currentTime.substring(0, 4).equals(lastPromptTime.substring(0, 4));
    }

    public void setLastPromptTime(String userId, String timestamp) {
        lastPromptTimeMap.put(userId, timestamp);
    }

    private String getCurrentTime() {
        LocalDateTime currentDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        return currentDateTime.format(formatter);
    }

    private static int getWeekOfYear(String timestamp) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDateTime dateTime = LocalDateTime.parse(timestamp, formatter);
        return dateTime.get(IsoFields.WEEK_OF_WEEK_BASED_YEAR);
    }

    // 示例用法
    public static void main(String[] args) {
        UserPrompt userPrompt = new UserPrompt();
        String userId = "123456";

        if (userPrompt.shouldPromptEveryTime(userId)) {
            System.out.println("弹出提示 - 每次必弹");
        }

        if (userPrompt.shouldPromptDaily(userId)) {
            System.out.println("弹出提示 - 每天一次");
        }

        if (userPrompt.shouldPromptWeekly(userId)) {
            System.out.println("弹出提示 - 每周一次");
        }

        if (userPrompt.shouldPromptMonthly(userId)) {
            System.out.println("弹出提示 - 每月一次");
        }

        if (userPrompt.shouldPromptYearly(userId)) {
            System.out.println("弹出提示 - 每年一次");
        }

        String timestamp = userPrompt.getCurrentTime();
        userPrompt.setLastPromptTime(userId, timestamp);
        System.out.println(timestamp);

        System.out.println(getWeekOfYear("20220508235959"));

        System.out.println(getWeekOfYear("20230508235959"));
    }
}
