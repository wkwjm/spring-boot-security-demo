package com.sans.common.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @title: PhoneNumberPrompt
 * @Author 王康
 * @Date: 2023/9/5 11:35
 * @Version 1.0
 */
public class PhoneNumberPrompt {
    private Map<String, Long> lastPromptTimeMap; // 存储每个手机号码对应的上次弹出提示的时间戳

    public PhoneNumberPrompt() {
        this.lastPromptTimeMap = new HashMap<>();
        lastPromptTimeMap.put("1234567890", 1693885167471L);
    }

    public boolean shouldPromptEveryTime(String phoneNumber) {
        return true; // 每次必弹
    }

    public boolean shouldPromptDaily(String phoneNumber) {
        long currentTime = System.currentTimeMillis();
        Date currentDate = new Date(currentTime);

        long lastPromptDate = getLastPromptDate(phoneNumber); // 上次弹出提示的日期
        long currentDay = currentTime / (24 * 60 * 60 * 1000); // 当前日期

        if (currentDay > lastPromptDate) {
            setLastPromptTime(phoneNumber, currentTime);
            return true;
        }

        return false;
    }

    public boolean shouldPromptWeekly(String phoneNumber) {
        long currentTime = System.currentTimeMillis();
        Date currentDate = new Date(currentTime);

        long lastPromptWeek = getLastPromptWeek(phoneNumber); // 上次弹出提示的周数
        long currentWeek = currentTime / (7 * 24 * 60 * 60 * 1000); // 当前周数

        if (currentWeek > lastPromptWeek) {
            setLastPromptTime(phoneNumber, currentTime);
            return true;
        }

        return false;
    }

    public boolean shouldPromptMonthly(String phoneNumber) {
        long currentTime = System.currentTimeMillis();
        Date currentDate = new Date(currentTime);

        int lastPromptMonth = getLastPromptMonth(phoneNumber); // 上次弹出提示的月份
        int currentMonth = currentDate.getMonth(); // 当前月份

        System.out.println(currentMonth);
        if (currentMonth > lastPromptMonth) {
            setLastPromptTime(phoneNumber, currentTime);
            return true;
        }

        return false;
    }

    public boolean shouldPromptYearly(String phoneNumber) {
        long currentTime = System.currentTimeMillis();
        Date currentDate = new Date(currentTime);

        int lastPromptYear = getLastPromptYear(phoneNumber); // 上次弹出提示的年份
        int currentYear = currentDate.getYear(); // 当前年份

        System.out.println(currentYear);
        if (currentYear > lastPromptYear) {
            setLastPromptTime(phoneNumber, currentTime);
            return true;
        }

        return false;
    }

    private long getLastPromptDate(String phoneNumber) {
        return lastPromptTimeMap.getOrDefault(phoneNumber, 0L) / (24 * 60 * 60 * 1000);
    }

    private long getLastPromptWeek(String phoneNumber) {
        return lastPromptTimeMap.getOrDefault(phoneNumber, 0L) / (7 * 24 * 60 * 60 * 1000);
    }

    private int getLastPromptMonth(String phoneNumber) {
        long lastPromptTime = lastPromptTimeMap.getOrDefault(phoneNumber, 0L);
        Date lastPromptDate = new Date(lastPromptTime);
        return lastPromptDate.getMonth();
    }

    private int getLastPromptYear(String phoneNumber) {
        long lastPromptTime = lastPromptTimeMap.getOrDefault(phoneNumber, 0L);
        Date lastPromptDate = new Date(lastPromptTime);
        return lastPromptDate.getYear();
    }

    private void setLastPromptTime(String phoneNumber, long timestamp) {
        lastPromptTimeMap.put(phoneNumber, timestamp);
    }

    // 示例用法
    public static void main(String[] args) {

        PhoneNumberPrompt phoneNumberPrompt = new PhoneNumberPrompt();

        String phoneNumber = "1234567890";

        if (phoneNumberPrompt.shouldPromptEveryTime(phoneNumber)) {
            System.out.println("弹出提示 - 每次必弹");
        }

        if (phoneNumberPrompt.shouldPromptDaily(phoneNumber)) {
            System.out.println("弹出提示 - 每天一次");
        }

        if (phoneNumberPrompt.shouldPromptWeekly(phoneNumber)) {
            System.out.println("弹出提示 - 每周一次");
        }

        if (phoneNumberPrompt.shouldPromptMonthly(phoneNumber)) {
            System.out.println("弹出提示 - 每月一次");
        }

        if (phoneNumberPrompt.shouldPromptYearly(phoneNumber)) {
            System.out.println("弹出提示 - 每年一次");
        }
    }

}
