package com.sans.common.util;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.swing.*;

import org.apache.commons.codec.binary.Base64;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
/**
 * @title: ChenrdBase64
 * @Author 王康
 * @Date: 2022/8/4 10:54
 * @Version 1.0
 */
public class ChenrdBase64 extends JFrame {
    private static final long serialVersionUID = 1L;
    Base64 base64 = new Base64();
    JPanel desktop = new JPanel();
    JPanel jfr1 = new JPanel();
    JPanel btnAllPanel = new JPanel();

    JButton incodeBtn = new JButton("加密");
    JButton decodeBtn = new JButton("解密");
    JButton formatBtn = new JButton("格式化");
    JLabel  jLabel = new JLabel("科大国创软件股份有限公司");;
    JTextArea inputMsg = new JTextArea("", 28, 60);

    public ChenrdBase64() {
        super("Base64加解密工具");
        this.add(desktop);
        this.setBounds(0, 0, 900, 600);
        this.setLocationRelativeTo(null);
        this.setLocationByPlatform(true);
        this.setVisible(true);
        init();
    }

    public void init() {
        inputMsg.setLineWrap(true);
        // 激活自动换行功能
        inputMsg.setWrapStyleWord(true);
        // 激活断行不断字功能
        JScrollPane js = new JScrollPane(inputMsg);

        jfr1.setLayout(new BorderLayout());
        jfr1.add(js, BorderLayout.NORTH);

        // 设置组件垂直排列
        btnAllPanel.add(incodeBtn);
        btnAllPanel.add(decodeBtn);
        btnAllPanel.add(formatBtn);
        jfr1.add(btnAllPanel, BorderLayout.SOUTH);
        desktop.add(jLabel,BorderLayout.SOUTH);
        desktop.add(jfr1, 0);

        // 换行
        inputMsg.setSelectedTextColor(Color.BLUE);
        // 激活自动换行功能
        inputMsg.setLineWrap(true);
        // 激活断行不断字功能
        inputMsg.setWrapStyleWord(true);


        // 给按钮添加点击事件
        incodeBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String msg = inputMsg.getText();
                    inputMsg.setText(new String(Base64.encodeBase64(msg.getBytes("utf-8"))));
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            }
        });
        // 给按钮添加点击事件
        decodeBtn.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                String msg = inputMsg.getText();
                try {
                    inputMsg.setText(new String(base64.decode(msg.getBytes("utf-8"))));
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }

            }
        });
        // 给按钮添加点击事件
        formatBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // 格式化输出格式(使用dom4j格式化)
                OutputFormat format = OutputFormat.createPrettyPrint();
                format.setEncoding("utf-8");
                StringWriter writer = new StringWriter();
                // 格式化输出流
                XMLWriter xmlWriter = new XMLWriter(writer, format);
                // 将document写入到输出流
                try {
                    String text = inputMsg.getText();
                    Document doc = DocumentHelper.parseText(text);
                    xmlWriter.write(doc);
                    inputMsg.setText(writer.toString());
                    System.out.println(writer.toString());
                    xmlWriter.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    public static void main(String[] args) {
        ChenrdBase64 my64 = new ChenrdBase64();
        my64.validate();// 刷新所有子组件
    }
}
