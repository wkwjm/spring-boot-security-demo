package com.sans.common.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

/**
 * @title: FileToBase64Converter
 * @Author 王康
 * @Date: 2023/9/7 20:35
 * @Version 1.0
 */
public class FileToBase64Converter {

    public static void main(String[] args) {
        String filePath = "D:\\王康\\王康-科大国创\\2023-08月需求\\1.png"; // 替换为实际的文件路径

        String base64String = convertFileToBase64(filePath);
        System.out.println(base64String);
    }

    public static String convertFileToBase64(String filePath) {
        try {
            Path path = Paths.get(filePath);
            byte[] fileBytes = Files.readAllBytes(path);
            byte[] base64Bytes = Base64.getEncoder().encode(fileBytes);
            return new String(base64Bytes);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
