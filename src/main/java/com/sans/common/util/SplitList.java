package com.sans.common.util;

/**
 * @title: SplitList
 * @Author 王康
 * @Date: 2023/8/26 15:10
 * @Version 1.0
 */
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SplitList {

    public static List<String> splitAndJoin(List<String> list) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < list.size(); i += 2) {
            int end = Math.min(i + 2, list.size());
            List<String> sublist = list.subList(i, end);
            String joined = String.join("|", sublist);
            result.add(joined);
        }
        return result;
    }

    public static void main(String[] args) {
        List<String> myList = new ArrayList<>();
        myList.add("111");
        myList.add("222");
        myList.add("333");
        myList.add("444");
        myList.add("555");
        myList.add("666");
        myList.add("777");
        myList.add("888");
        myList.add("999");



        List<String> result = splitAndJoin(myList);
        System.out.println(result);
        // 输出: [HelloWorld, JavaProgramming, isfun]
        String idCard = "12w41243123124fgsdgsgx身份证号码：32012319901234567X";
        String desensitizedIdCard = idCard.replaceAll("(\\d{6})\\d{8}(\\w{4})", "$1********$2");
        System.out.println("脱敏后的身份证号码：" + desensitizedIdCard);

        System.out.println(maskIDCard(idCard));


        LocalDateTime now = LocalDateTime.now(); // 获取当前时间
        LocalDateTime fiveDaysLater = now.plusDays(5); // 获取 5 天后的时间
        LocalDateTime wk = fiveDaysLater.minusSeconds(1); // 将 5 天后的时间减一秒

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss"); // 定义日期时间格式化器
        String formattedDateTime = wk.format(formatter); // 格式化日期时间为字符串
        System.out.println(now.format(formatter));
        System.out.println("5 天后的当前时间减一秒为：" + formattedDateTime);


    }

    /**
     * 身份证号码脱敏处理
     *
     * @param line 日志行
     * @return 脱敏后的日志行
     */
    public static String maskIDCard(String line) {
        // 使用正则表达式匹配身份证号码
        String regex = "\\d{17}(\\d|x|X)";
        line = line.replaceAll(regex, "**************"); // 将身份证号码替换为固定长度的星号

        return line;
    }
}
