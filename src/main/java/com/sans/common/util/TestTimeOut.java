package com.sans.common.util;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @title: TestTimeOut
 * @Author 王康
 * @Date: 2023/8/3 9:24
 * @Version 1.0
 */
@Slf4j
public class TestTimeOut {

    private static final CountDownLatch countDownLatch = new CountDownLatch(5);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        long startTime = System.currentTimeMillis();
        String phoneNo = "18756968631";
        //1、随心选权益
        Map<String,Object> a_1 = querySxxInfo(phoneNo);
        //2、石化权益
        Map<String,Object> b_1 = queryShihuaInfo(phoneNo);
        //3、移动丰原
        Map<String,Object> c_1 = queryFengyuanInfo(phoneNo);
        //4、5G权益
        Map<String,Object> d_1 = query5GInfo(phoneNo);
        //5、青春卡
        Map<String,Object> e_1 = queryYangInfo(phoneNo);

        List<Map<String,Object>> result1 = new ArrayList<>();
        result1.add(a_1);
        result1.add(b_1);
        result1.add(c_1);
        result1.add(d_1);result1.add(e_1);

        long endTime_one = System.currentTimeMillis();
        System.out.println("未使用多线程耗时:"+(endTime_one-startTime)/1000 +"秒");
        System.out.println("未使用多线程耗时:"+(endTime_one-startTime) +"毫秒");
        System.out.println(JSON.toJSONString(result1));




        ExecutorService executorService = Executors.newFixedThreadPool(5);
        CountDownLatch countDownLatch = new CountDownLatch(5);
        List<Future<Map<String, Object>>> futures = new ArrayList<>();
        try{
            Future<Map<String, Object>> a = executorService.submit(() -> querySxxInfo(phoneNo));
            futures.add(a);
        }catch (Exception e){
            log.error("error",e);
        }finally {
            countDownLatch.countDown();
        }

        try{
            Future<Map<String, Object>> a = executorService.submit(() -> queryShihuaInfo(phoneNo));
            futures.add(a);
        }catch (Exception e){
            log.error("error",e);
        }finally {
            countDownLatch.countDown();
        }

        try{
            Future<Map<String, Object>> a = executorService.submit(() -> queryFengyuanInfo(phoneNo));
            futures.add(a);
        }catch (Exception e){
            log.error("error",e);
        }finally {
            countDownLatch.countDown();
        }
        try{
            Future<Map<String, Object>> a = executorService.submit(() -> query5GInfo(phoneNo));
            futures.add(a);
        }catch (Exception e){
            log.error("error",e);
        }finally {
            countDownLatch.countDown();
        }
        try{
            Future<Map<String, Object>> a = executorService.submit(() -> queryYangInfo(phoneNo));
            futures.add(a);
        }catch (Exception e){
            log.error("error",e);
        }finally {
            countDownLatch.countDown();
        }

        // 等待所有线程执行完毕
        countDownLatch.await();
        System.out.println("All threads have finished");

        executorService.shutdown();
        List<Map<String,Object>> result = new ArrayList<>();
        for (Future<Map<String, Object>> future : futures){
            result.add(future.get());
        }
        long endTime = System.currentTimeMillis();

        System.out.println("使用多线程耗时:"+(endTime-endTime_one)/1000 +"秒");
        System.out.println("使用多线程耗时:"+(endTime-endTime_one) +"毫秒");
        System.out.println(JSON.toJSONString(result));
    }

    private static Map<String, Object> queryYangInfo(String phoneNo) throws InterruptedException {
        Map<String,Object> map = new HashMap<>();
        map.put("name","e");
        Thread.sleep(3000);
        return map;
    }

    private static Map<String, Object> query5GInfo(String phoneNo) throws InterruptedException {
        Map<String,Object> map = new HashMap<>();
        map.put("name","d");
        Thread.sleep(2000);
        return map;
    }

    private static Map<String, Object> queryFengyuanInfo(String phoneNo) throws InterruptedException {
        Map<String,Object> map = new HashMap<>();
        map.put("name","c");
        Thread.sleep(1000);
        return map;
    }

    private static Map<String, Object> queryShihuaInfo(String phoneNo) throws InterruptedException {
        Map<String,Object> map = new HashMap<>();
        map.put("name","b");
        Thread.sleep(3000);
        return map;
    }

    private static Map<String, Object> querySxxInfo(String phoneNo) throws InterruptedException {
        Map<String,Object> map = new HashMap<>();
        map.put("name","a");
        Thread.sleep(1000);
        return map;
    }
}
