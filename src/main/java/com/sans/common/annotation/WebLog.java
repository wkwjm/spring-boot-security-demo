package com.sans.common.annotation;

import java.lang.annotation.*;

/**
 * @title: WebLog 使用自定义注解，AOP 切面统一打印出入参日志
 * @Author 王康
 * @Date: 2022/6/30 11:49
 * @Version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface WebLog {
    /**
     * 日志描述信息
     *
     * @return
     */
    String desc() default "";

}
