package com.sans.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sans.core.entity.SysMenuEntity;

/**
 * @Description 权限业务接口
 * @Author wk
 * @CreateTime 2021/9/14 15:57
 */
public interface SysMenuService extends IService<SysMenuEntity> {

}