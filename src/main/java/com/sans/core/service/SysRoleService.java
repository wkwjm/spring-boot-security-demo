package com.sans.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sans.core.entity.SysRoleEntity;

/**
 * @Description 角色业务接口
 * @Author wk
 * @CreateTime 2021/9/14 15:57
 */
public interface SysRoleService extends IService<SysRoleEntity> {

}